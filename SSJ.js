let SSJInterval = {
  intervalId: null,
  checkSSJ: true,

  start: function() {
    if (this.checkSSJ && GAME.quick_opts.ssj) {
      this.intervalId = setInterval(() => {
        this.checkTR();
      }, 2700 * 1 * 1);
    }
  },

  stop: function() {
    clearInterval(this.intervalId);
  },

  checkTR: function() {
    if (this.checkSSJ && GAME.quick_opts.ssj) {
      if ($("#ssj_bar")[0].attributes[2].value == "display: none;") {
        GAME.emitOrder({ a: 18, type: 5, tech_id: GAME.quick_opts.ssj[0] });
      } else if ($('#ssj_status').text() == "--:--:--") {
        GAME.emitOrder({ a: 18, type: 6 }); // Wyłącza SSJ
        window.setTimeout(() => this.checkTR(), 100);
      }
    }
  },
};

// Wywołanie funkcji start() dla rozpoczęcia sprawdzania SSJ
SSJInterval.start();
