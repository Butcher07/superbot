class SuperAsystent {
    constructor() {
        this.index = 0;
        this.milliseconds = 300;
        this.deactivatedEMP = [];
        this.page = 1;
        this.eq_page = 0;
        this.employee_10 = [];
        this.assistDataArray = [];
        this.panelHTML = `
            <div style="position: fixed; top: 50px; right: 20px; background-color: #f5f5f5; border: 1px solid #ccc; padding: 10px; z-index: 9999;">
                <h3>Wybierz opcję:</h3>
                <select id="optionSelector">
                    <option value="turnieje">Zapisz Tuty</option>
                    <option value="wyprawa">Wyprawa</option>
                    <option value="trenuj">Trenuj</option>
                    <option value="asysty">Asysty</option>
                    <option value="pracownik">Pracownik</option>
                </select>
                <button id="superAI"; sAI.index=0; onclick="sAI.getPlayerID()">Wykonaj</button>
            </div>
        `;
    }

    setOrder() {
        GAME.Order = GAME[Object.getOwnPropertyNames(GAME).find(v => /^.*Order/.test(v))];
    }

    addPanel() {
        document.body.insertAdjacentHTML('beforeend', this.panelHTML);
    }

    async start() {
        if (this.index < this.playerIDs.length) {
            setTimeout(() => {
                this.loguj();
            }, 1200);
        } else if ((document.getElementById('optionSelector').value) == 'wyprawa') {
            this.index = 0;
            await this.delay(200);
            this.start();
        }
        else if (this.employee_10.length >= 1) {
            console.log('Wykwalifikuj pracownika na tych postaciach ' + this.employee_10);
        }
        else if (document.getElementById('optionSelector').value == 'pracownik' && this.deactivatedEMP.length >= 1) {
            console.log('Aktywuj Pracownika na tych postaciach ' + this.deactivatedEMP);
        }
        else {
            GAME.komunikat('The task is complete, my Lord. Your will has been executed with precision');
        }
    }

    async loguj() {
        let char_id = parseInt(this.playerIDs[this.index]);
        GAME.Order({ a: 2, char_id: char_id });
        this.index += 1;
        console.log(this.index);
        setTimeout(() => {
            this.handleOption(document.getElementById('optionSelector').value);
        }, 400);
    }

    async getPlayerID() {
        this.playerIDs = [];
        console.log(this.playerIDs);
        $("li[data-option='select_char']").each((index, element) => {
            const charId = $(element).data("char_id");
            this.playerIDs.push(charId);
        });
        await this.start();
    }

    async handleOption(option) {
        switch (option) {
            case 'wyprawa':
                console.log('Wybrano opcję: Wyprawa!');
                await this.delay(this.milliseconds);
                this.doExpeditions();
                break;

            case 'trenuj':
                console.log('Wybrano opcję: Trenuj!');
                await this.delay(this.milliseconds);
                this.checkBonus();
                break;

            case 'asysty':
                console.log('Obsługa opcję: Asysty!');
                await this.delay(this.milliseconds);
                this.doAssists();
                break;

            case 'pracownik':
                console.log('Wybrano Opcję: Pracownik');
                await this.delay(this.milliseconds);
                this.doInstances();
                break;
            case 'turnieje':
                console.log('Wybrano Opcję: Zapisz Tutki');
                await this.delay(this.milliseconds);
                this.loadTour();

            default:
                break;
        }
        //await this.start();
    }

    formula(x) {
        const st = GAME.char_data.sila;
        const sp = GAME.char_data.szyb;
        const en = GAME.char_data.wytrz;
        const sw = GAME.char_data.swoli;
        const ki = GAME.char_data.ki;
        let res, formula, wta, tree;

        switch (x) {
            case 0:
                formula = [st * 0.55, sp * 0.80, en * 0.75, sw * 6, ki * 8];

                res = $.inArray(Math.min(...formula), [...formula]) + 1;
                return res;
            case 1:
            case 2:
                formula = [st * 0.65, sp * 0.80, en * 0.65, sw * 2, ki * 8];

                res = $.inArray(Math.min(...formula), [...formula]) + 1;
                return res;
            case 3:
                wta = GAME.char_data.wta;
                formula = [st * 0.75, sp * 0.85, en * 0.65, sw * 3, ki * 8, wta * 0.80];

                res = $.inArray(Math.min(...formula), [...formula]) + 1;
                return res;
            case 4:
                wta = GAME.char_data.wta;
                tree = (GAME.char_data.divine_tree === 3) ? 4 : (GAME.char_data.divine_tree === 4) ? 5 : GAME.char_data.divine_tree;

                formula = [st * 0.85, sp * 0.90, en * 0.85, sw * 0.25, ki * 8, wta * 0.80];

                res = $.inArray(Math.min(...formula), [...formula]) + 1;
                return res;
        }
    }

    async checkBonus() {
        await this.delay(1000);
        let res;
        if (GAME.char_data.bonus1 - GAME.getTime() <= 0) {
            res = this.buyBonuse(1);
            this.checkBonus();
        } else if (GAME.char_data.bonus14 - GAME.getTime() <= 0) {
            res = this.buyBonuse(14);
            this.checkBonus();
        } else if (GAME.char_data.bonus11 - GAME.getTime() <= 0) {
            res = this.buyBonuse(11);
            this.checkBonus();
        } else {
            this.setTrain();
        }
    }

    async setTrain() {
        await this.delay(300);
        const stat = this.formula(GAME.char_data.reborn);
        if (stat === undefined) {
            this.start();
        }  else if (GAME.timed < 2) {
            GAME.Order({ a: 8, type: 2, stat: stat, duration: 12 });
            this.setTrain();
        } else if (GAME.timed === 2) {
            this.start();
        }
    }

    /*async openBuffs() {
        GAME.Order({ a: 12, page: 10, page2: 0 });
        await this.delay(500);
        później to napiszę jak ma być xd
        let buff = $("#ekw_page_items").find(`div[data-base_item_id='${1753}']`);
        buff.attr("data-item_id");
    }*/

    async buyBonuse(x) {
        this.delay(this.milliseconds);
        GAME.Order({ a: 25, type: 5, id: x, c: 2, am: 4 });
    }


    delay(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    async doInstances() {
        this.eq_page = 0;
        GAME.Order({ a: 44, type: 0 });
        await this.delay(1500);

        if (GAME.emps.length <= 0) {
            await this.delay(800);
            this.start();
        }
        else if (GAME.char_data.level <= 500) {
            await this.delay(800);
            this.start();
        }
        else if (GAME.emps[0].active || GAME.emps[1].active == 1) {
            await this.delay(800);
            this.essenceStack();
        }
        else {
            await this.delay(800);
            console.log('Wychodzę!');
            this.start();
        }
    }

    async essenceStack() {

        GAME.Order({ a: 12, page: 1, page2: this.eq_page });
        await this.delay(1000);
        this.magicEssence = parseInt($(`#ekw_page_items div[data-base_item_id='${74}']`).attr("data-stack"));

        if (this.magicEssence <= 30) {
            GAME.Order({ a: 25, type: 4, id: 1, c: 2, am: 5 });
            await this.delay(500);
            this.instance();
        }
        if (isNaN(this.magicEssence) && this.eq_page >= 2) {
            GAME.Order({ a: 25, type: 4, id: 1, c: 2, am: 5 });
            await this.delay(500);
            this.instance();
        } else if (isNaN(this.magicEssence)) {
            this.eq_page++;
            await this.delay(500);
            this.essenceStack();
        } else if (this.magicEssence >= 30) {
            await this.delay(500);
            this.instance();
        }
        console.log(this.magicEssence);
    }

    async instance() {
        let actEmp = [GAME.emps[0], GAME.emps[1]].find(obj => obj && obj.active === 1);
        if (actEmp.energy <= 0) {
            GAME.Order({ a: 44, type: 9, emp: id });
            console.log('Odnawiam Energię!')
            await this.delay(200);
            this.instance();
        }
        else if (actEmp.exp > GAME.employe_exp(actEmp.level + 1)) {
            GAME.Order({ a: 44, type: 2, emp: actEmp.id });
            await this.delay(200);
            this.instance();
        }
        else if (GAME.char_data.icd_1 < 2) {
            GAME.Order({ a: 44, type: 8, emp: id, inst: 1 });
            console.log('Insta nr1 zrobiona!')
            await this.delay(200);
            this.instance();
        }
        else if (GAME.char_data.icd_2 < 2) {
            GAME.Order({ a: 44, type: 8, emp: id, inst: 2 });
            console.log('Insta nr2 zrobiona!')
            await this.delay(200);
            this.instance();
        }
        else if (GAME.char_data.icd_3 < 2) {
            GAME.Order({ a: 44, type: 8, emp: id, inst: 3 });
            console.log('Insta nr3 zrobiona!')
            await this.delay(200);
            this.instance();
        }
        else if (GAME.char_data.icd_4 < 2) {
            GAME.Order({ a: 44, type: 8, emp: id, inst: 4 });
            console.log('Insta nr4 zrobiona!')
            await this.delay(200);
            this.instance();
        }
        else if (GAME.char_data.icd_5 < 2) {
            GAME.Order({ a: 44, type: 8, emp: id, inst: 5 });
            console.log('Insta nr5 zrobiona!')
            await this.delay(200);
            this.instance();
        }
        else if (GAME.char_data.icd_6 < 2) {
            GAME.Order({ a: 44, type: 8, emp: id, inst: 6 });
            console.log('Insta nr6 zrobiona!')
            await this.delay(200);
            this.instance();
        }
        else if (actEmp.level >= 10 && !actEmp.qualified) {
            this.employee_10.push(GAME.char_data.name);
            await this.delay(200);
            this.start();
        }
        else {
            this.start();
        }

    }

    async doExpeditions() {

        if ((GAME.char_data.bonus16 - GAME.getTime() <= 0) && GAME.char_data.kk >= 10) {
            GAME.Order({ a: 25, type: 5, id: 16, c: 2, am: 4 });
            await this.delay(300);
            this.doExpeditions();
        }
        else if ((GAME.char_data.bonus16 - GAME.getTime() <= 0) && GAME.char_data.kk <= 10) {
            this.start();
        }
        else if (GAME.timed < 2) {
            GAME.Order({ a: 10, type: 2, ct: 0 });
            await this.delay(300);
            this.doExpeditions();
        }
        else {
            this.start();
        }
    }

    async doAssists() {
        if (GAME.char_data.reborn < 1) {
            this.index = 100;
            this.start();
        } else if (GAME.char_data.klan_id == 0) {
            this.start();
        }
        else {
            GAME.Order({ a: 39, type: 0 });
            $('.page_switch').hide();
            $('#page_game_klan').show();
            $('.clan_inner_page').hide();

            await this.delay(200);

            $('#clan_inner_' + 'ktrain').show();
            GAME.Order({ a: 39, type: 54 });

            await this.delay(200);
            this.freeAssist();
        }

    }
    async freeAssist() {
        let clanPlayerData = [];
        let assistData = [];
        GAME.Order({ a: 39, type: 15 });

        setTimeout(() => {
            for (const player of GAME.clan_players) {
                if (player && typeof player === 'object' && 'id' in player) {
                    const playerData = {
                        id: player.id,
                        reborn: player.reborn,
                        name: player.name
                    };
                    clanPlayerData.push(playerData);
                }
            }
            //console.log(clanPlayerData);

            const rows = document.querySelectorAll('tr');

            // Filtrujemy wiersze, pozostawiając tylko te, które mają nie-null assistTid
            const filteredRows = Array.from(rows).filter(row => {
                const assistButton = row.querySelector('button.btn_small_gold.option[data-option="clan_assist"]');
                if (assistButton) {
                    const assistTid = assistButton.getAttribute('data-tid');
                    return assistTid !== null;
                }
                return false;
            });

            filteredRows.forEach(row => {
                const charIdElement = row.querySelector('b.orange.option[data-option="show_player"][data-char_id]');

                if (charIdElement) {
                    const charId = charIdElement.getAttribute('data-char_id');

                    const timerElement = row.querySelector('.timer[data-end]');
                    if (timerElement) {
                        const timerEnd = timerElement.getAttribute('data-end') - GAME.getTime();

                        const assistButton = row.querySelector('button.btn_small_gold.option[data-option="clan_assist"]');
                        let assistTid = null;
                        if (assistButton) {
                            assistTid = assistButton.getAttribute('data-tid');
                        }

                        // Tworzymy obiekt z danymi i dodajemy go do tablicy this.assistDataArray
                        const data = {
                            charId: parseInt(charId),
                            timerEnd: timerEnd,
                            assistTid: parseInt(assistTid)
                        };
                        this.assistDataArray.push(data);

                        console.log(`Char ID: ${charId}, Timer End: ${timerEnd}, Assist Tid: ${assistTid}`);
                    }
                }
            });

            // Dodaj reborn do this.assistDataArray na koniec pętli filteredRows.forEach
            this.assistDataArray.forEach(data => {
                const matchingPlayerData = clanPlayerData.find(player => player.id === data.charId);
                if (matchingPlayerData) {
                    data.reborn = matchingPlayerData.reborn;
                    data.name = matchingPlayerData.name;
                }
            });
            console.log(this.assistDataArray);
            this.taskManager();
        }, 500);
    }

    async emitAssist() {
        // Sprawdź, czy this.assistDataArray ma jeszcze dane
        console.log(this.assistDataArray)
        if (this.assistDataArray.length > 0) {
            const data = this.assistDataArray.pop(); // Usuwamy i pobieramy ostatni element
            const assistTid = data.assistTid;
            const charId = data.charId;

            GAME.Order({ a: 39, type: 55, tid: assistTid, target: charId });

            await this.delay(2000);
            this.emitAssist();


        } else {
            console.log('Brak dostępnych danych do przetworzenia. Emit zostanie zakończony.');
            this.start()
        }
    }

    async taskManager() {
        console.log(this.assistDataArray)
        const butcher = this.assistDataArray.find(({ name }) => name === "ZephyR");
        const butcherInd = this.assistDataArray.findIndex(({ name }) => name === "ZephyR");

        if (this.assistDataArray.find(({ name }) => name === "Granolah") != undefined) {
            this.assistDataArray.splice(this.assistDataArray.findIndex(({ name }) => name === "Granolah"), 1);
        }

        if (!butcher) {
            this.emitAssist();
        } else if (butcher.timerEnd <= 500) {
            GAME.Order({ a: 39, type: 56 })
            this.start();
        } else if (butcher.timerEnd >= 3300) {
            this.assistDataArray.splice(butcherInd, 1);
            this.emitAssist();
        } else {
            this.emitAssist();
        }
    }

    async loadTour() {
        await this.delay(4000);
        GAME.Order({ a: 57, type: 0, type2: 0, page: this.page });
        $('.page_switch').hide();
        $('#page_game_tournaments').show();
        await this.delay(4000);
        this.page = 1;
        this.tournaments();
    }

    async tournaments() {
        if ($("button[data-option='tournament_sign']").length == 1) {
            GAME.Order({ a: 57, type: 1, tid: $("button[data-option='tournament_sign']").attr("data-tid") });
            await this.delay(2000);
            this.start();
        } else {
            this.page = 2;
            await this.delay(1000);
            this.loadTour();
        }
    }

}

let sAI = new SuperAsystent();
$(document).ready(function () {
    $("#top_bar a").remove();
    var topBar = $("#top_bar");

    function addMenuItem(text) {
        var menuItem = $("<a class='newTopBar'>" + text + "</a>");
        topBar.prepend(menuItem);

        menuItem.on("click", function () {
            var menuItem = $(this);

            // Usuń podkreślenie tekstu w odnośniku
            menuItem.css("text-decoration", "none");

            // Sprawdź, czy kliknięto w "Aktywności"
            if (menuItem.text() === "Aktywności") {
                menuItem.html('<span class="pulseText">Aktywności</span>');

                setTimeout(function () {
                    menuItem.html('<span class="highlightText">Aktywności</span>');
                    activities();
                }, 5000);
            } else if (menuItem.text() === "Kody") {
                menuItem.html('<span class="pulseText">' + text + '</span>');

                setTimeout(function () {
                    menuItem.html('<span class="highlightText">' + text + '</span>');

                    setInterval(() => { KODY.upgradeTraining(); }, 5000);
                }, 5000);
            }
            else if (menuItem.text() === "Instancje") {
                // Dodaj animację CSS bezpośrednio do tekstu innych elementów
                menuItem.html('<span class="pulseText">' + text + '</span>');

                setTimeout(function () {

                    menuItem.html('<span class="highlightText">' + text + '</span>');

                    var en;
                    GAME.socket.emit('ga', { a: 44, type: 0 });
                    setTimeout(() => { insty(); }, 500);
                }, 5000);
            }
            else if (menuItem.text() === "Hiper Asystent") {
                // Dodaj animację CSS bezpośrednio do tekstu innych elementów
                menuItem.html('<span class="pulseText">' + text + '</span>');

                setTimeout(function () {
                    menuItem.html('<span class="highlightText">' + text + '</span>');
                }, 5000);
            }
            else if (menuItem.text() === "Handlarz") {
                // Dodaj animację CSS bezpośrednio do tekstu innych elementów
                menuItem.html('<span class="pulseText">' + text + '</span>');

                setTimeout(function () {
                    menuItem.html('<span class="highlightText">' + text + '</span>');
                    handlarz();
                }, 5000);
            }
            else if (menuItem.text() === "Asystent") {
                // Dodaj animację CSS bezpośrednio do tekstu innych elementów
                menuItem.html('<span class="pulseText">' + text + '</span>');

                setTimeout(function () {
                    menuItem.html('<span class="highlightText">' + text + '</span>');
                    sAI.setOrder();
                    sAI.addPanel();
                }, 5000);
            }
        });
    }

    // Dodaj elementy do paska nawigacji
    addMenuItem("Aktywności");
    addMenuItem("Kody");
    addMenuItem("Instancje");
    addMenuItem("Hiper Asystent");
    addMenuItem("Handlarz");
    addMenuItem("Asystent");

    function activities() {
        let points = parseInt($('#char_activity').text());
        let act_prizes = document.querySelector("#act_prizes").children;

        if (isNaN(points)) {
            GAME.Order({ a: 49, type: 0 });
            window.setTimeout(activities, 400);
        } else if (points >= 25 && !act_prizes[0].classList.contains('disabled')) {
            activitiesHelper(0);
        } else if (points >= 50 && !act_prizes[1].classList.contains('disabled')) {
            activitiesHelper(1);
        } else if (points >= 75 && !act_prizes[2].classList.contains('disabled')) {
            activitiesHelper(2);
        } else if (points >= 100 && !act_prizes[3].classList.contains('disabled')) {
            activitiesHelper(3);
        } else if (points >= 150 && !act_prizes[4].classList.contains('disabled')) {
            activitiesHelper(4);
        } else {
            console.log('Odebrano Wszystkie aktywności');
             $(".newTopBar:contains('Aktywności')").find('.highlightText').css('color', 'white');
        }

    }

    function activitiesHelper(x) {
        GAME.Order({ a: 49, type: 0 });
        setTimeout(() => {
            GAME.Order({ a: 49, type: 1, ind: x });
            console.log(`Odebrano ${x + 1} nagrode`);
            window.setTimeout(activities, 200);
        }, 100);
    }

    function handlarz() {
        /*
1925 - piguła max normal
1926 - piguła max rare
1927 - piguła max unique
1928 - piguła max elite
1929 - piguła max super unique

1930 - piguła przyrost normal
1931 - piguła przyrost rare
1932 - piguła przyrost unique
1933 - piguła przyrost elite
1934 - piguła przyrost super unique

1936 - sfera normal
1937 - sfera rare
1938 - sfera unique

1243 - czerwone senzu
1784 - karta dusz
1935 - tytuł
1941 - KK
*/
        let controlHandler = false;
        const wanted = [1938, 1937, 1936, 1243, 1934, 1933, 1929, 1928, 1932, 1927, 1931, 1926, 1930, 1925, 1243]; // lista pożądanych itemów, id kolejno od najbardzej pożądanego do najmniej

        const getTraderItems = (cb) => {
            let trader_items = [];
            $(`button[data-option="buy_from_trader2"]`).each(function () {
                let index = parseInt($(this).attr("data-item"));
                let item = parseInt($(this).attr("data-itemid"));
                let am = parseInt($(this).attr("data-itemam"));
                if (wanted.includes(item)) {
                    trader_items.push({ index: index, item: item, am: am });
                }
            });

            cb(trader_items);
        }

        const buyTraderItems = (items) => {
            if (items.length > 0 && controlHandler) {
                items.sort(function (a, b) { return wanted.indexOf(a.item) - wanted.indexOf(b.item); });
                //$("button[data-item='"+items[0].index+"']").remove();
                GAME.socket.emit('ga', { a: 51, type: 3, item: items[0].index, iid: items[0].item, am: items[0].am });
                setTimeout(() => { getTraderItems((items) => { buyTraderItems(items) }) }, 50);
            } else if (!$('#trader_goods2 .trade_good').length && controlHandler) {
                GAME.socket.emit('ga', { a: 51, type: 0 });
                setTimeout(() => { getTraderItems((items) => { buyTraderItems(items) }) }, 50);
            } else if ($('#trader_goods2 .trade_good').length && !items.length && controlHandler) {
                console.log("Brak pożądanych itemów");
            }
        }

        //getTraderItems((items)=>{ buyTraderItems(items) });

        let checkTimeHandler = setInterval(() => {
            const now = new Date();
            const currHour = now.getHours();
            const currMinute = now.getMinutes();
            //console.log(currHour, currMinute);

            if (currHour >= 19 && currMinute >= 50 && controlHandler === false) {
                controlHandler = true;
                getTraderItems((items) => { buyTraderItems(items) });
            } else if (currHour >= 20 && currMinute >= 10) {
                controlHandler = false;
                $(".newTopBar:contains('Handlarz')").find('.highlightText').css('color', 'white');
                clearInterval(checkTimeHandler);
            }
            else { console.log('Czekam na Handlarza!') }
        }, 2000);


    }

    function insty() {
        if (en == undefined) {
            GAME.socket.emit('ga', { a: 44, type: 0 });
            window.setTimeout(insty, 100);
        } else if (en == 0) {
            GAME.socket.emit('ga', { a: 44, type: 0 });
            GAME.socket.emit('ga', { a: 44, type: 9, emp: id });
            window.setTimeout(insty, 100);
        } else if (GAME.char_data.icd_1 < 2) {
            GAME.socket.emit('ga', { a: 44, type: 0 });
            GAME.socket.emit('ga', { a: 44, type: 8, emp: id, inst: 1 });
            window.setTimeout(insty, 100);
        } else if (GAME.char_data.icd_2 < 2) {
            GAME.socket.emit('ga', { a: 44, type: 0 });
            GAME.socket.emit('ga', { a: 44, type: 8, emp: id, inst: 2 });
            window.setTimeout(insty, 100);
        } else if (GAME.char_data.icd_3 < 2) {
            GAME.socket.emit('ga', { a: 44, type: 0 });
            GAME.socket.emit('ga', { a: 44, type: 8, emp: id, inst: 3 });
            window.setTimeout(insty, 100);
        } else if (GAME.char_data.icd_4 < 2) {
            GAME.socket.emit('ga', { a: 44, type: 0 });
            GAME.socket.emit('ga', { a: 44, type: 8, emp: id, inst: 4 });
            window.setTimeout(insty, 100);
        } else if (GAME.char_data.icd_5 < 2) {
            GAME.socket.emit('ga', { a: 44, type: 0 });
            GAME.socket.emit('ga', { a: 44, type: 8, emp: id, inst: 5 });
            window.setTimeout(insty, 100);
        } else if (GAME.char_data.icd_6 < 2) {
            GAME.socket.emit('ga', { a: 44, type: 0 });
            GAME.socket.emit('ga', { a: 44, type: 8, emp: id, inst: 6 });
            window.setTimeout(insty, 100);
        } else {
            GAME.komunikat("Wykonano wszystkie instancje");
            $(".newTopBar:contains('Instancje')").find('.highlightText').css('color', 'white');
            return;
        }

    }

    const KODY_CONFIG = { multi: true, useSSJ: true }

    class RC_KODY {
        constructor(data) {
            this.conf = data;
            //setInterval(() => {this.upgradeTraining();}, 5000);
        }

        upgradeTraining() {
            if (GAME.is_training && GAME.char_data.train_ucd - GAME.getTime() <= 0) {
                GAME.socket.emit('ga', { a: 8, type: 5, multi: this.conf.multi, apud: 'vzaaa' });
            }
            else {
                (console.log('Czekaj!', 'Take it easy'));
                if (this.conf.useSSJ === true) {
                    this.ssj();
                }
            }
        }

        ssj() {
            if ($("#ssj_bar").css("display") === "none") {
                GAME.emitOrder({ a: 18, type: 5, tech_id: GAME.quick_opts.ssj[0] });
            } else if ($('#ssj_status').text() == "--:--:--") {
                GAME.emitOrder({ a: 18, type: 6 });
            }
        }

    }
    GAME.emitOrder = (data) => GAME.socket.emit('ga', data);

    const KODY = new RC_KODY(KODY_CONFIG);

});

const newTopBarCSS = `.newTopBar {
    text-shadow: -1px -1px 10px #000, 1px -1px 10px #000, -1px 1px 10px #000, 1px 1px 10px #000;
    font-family: 'Play', sans-serif;
    font-size: 13px;
    text-transform: uppercase;
    font-weight: bold;
    color: white !important;
    padding: 0px 5px 0px 5px;
    cursor: pointer;
    transition: background-color 0.5s;
}

.pulseText {
    animation: pulse 2s infinite;
}

.highlightText {
    color: #E9B824;
    transition: color 0.5s;
}

.newTopBar:hover {
    color: white !important;
}

.newTopBar:first-of-type {
    padding-left: 2%;
}

@keyframes pulse {
    0% { color: white; }
    50% { color: red; }
    100% { color: white; }
}

.active newTopBar{
    background-color: #FFD700;
    color: white !important;
}

a.newTopBar {
    text-decoration: none;
}
`;

document.body.insertAdjacentHTML('beforeend', '<style>' + newTopBarCSS + '</style>');
//211537
