let SoulInterval = {
  intervalId: null,
  arenaPlayerIndex: 1,

  start: function() {
    if (!this.intervalId) {
      this.intervalId = setInterval(() => {
        this.soulFight();
      }, 6000);
    }
  },

  stop: function() {
    if (this.intervalId) {
      clearInterval(this.intervalId);
      this.intervalId = null;
    }
  },

  soulFight: function() {
    GAME.socket.emit('ga',{ a: 59, type: 0 });
    if ($("#ss_cd_still")[0].style[0] == "display") {
      console.log("soul fight");
      window.setTimeout(() => this.soulFightGameFunction(), 1000);
      window.setTimeout(() => this.closeFight(), 2000);
    }
  },

  soulFightGameFunction: function() {
    GAME.socket.emit('ga',{ a: 59, type: 1 });
  },

  closeFight: function() {
    clearInterval(GAME.fight_timer);
    $('#fight_view').fadeOut();
  }
};

// Wywołanie funkcji start() dla rozpoczęcia interwału soulFight
SoulInterval.start();
