setTimeout(() => { GAME.socket.emit('ga', { a: 12, page: GAME.ekw_page, page2: GAME.ekw_page2, used: 1 }); }, 39);


var bb1 = false;     //Błogosławieństwo Widmowego Smoka | data-buff=100 | data-base_item_id="1801"
var bb2 = false;     //Grzech Pychy | 150 000 do max PA | data-buff=96 | data-base_item_id="1796"
var bb3 = false;     //Grzech Obżarstwa | 100% Większy limit Blue Senzu [data-buff=95] |
var bb4 = false;     //Grzech Pożądania | 5% do szansy na moc z walk PvM |data-buff=94 |
var bb5 = false;     //Grzech Lenistwa | 200% do przyrostu | data-buff="93" | 
var bb6 = false;     //Grzech Chciwości | 5% do szansy na zdobycie PSK | data-buff="92" |
var bb7 = false;     //Grzech Zazdrości | 10% do szansy na zdobycie Skutera | data-buff="91"|
var bb8 = false;     //Grzech Gniewu | 200% do doświadczenia | data-buff="90"
var bb9 = false;     //Błogosławieństwo Leniwej Boginii | 300% do przyrostu Punktów Akcji | data-buff="82"
var bb10 = false;    //Błogosławieństwo Szalonej Bogini | 100 % większy limit dzienny Niebieskich Senzu | data-buff="81"
var bb11 = false;    //Błogosławieństwo Życzliwej Bogini | 5 % do szansy na podwójnie efektywny bonus za ulepszenie treningu | data-buff="80"
var bb12 = false;    //Błogosławieństwo Legendarnego Bohatera | 100 % do sławy za walki w wojnach imperiów |data-buff="73"
var bb13 = false;    //Błogosławieństwo Boga Wojny | 5 minut(y) krótszy cooldown między walkami PvP | data-buff="71" 
var bb14 = false;    //Zaklęta esencja mocy | 5 % do szansy na moc z walk PvM | data-buff= 55 
var bb15 = false;    //Zaklęta esencja inspiracji | 250 % do efektywności treningu | data-buff= 54 
var bb16 = false;    //Zaklęta esencja szczęścia | 5 % do szansy na 3x więcej doświadczenia za wygrane walki PvM | data-buff= 53
var bb17 = false;    //Zaklęta esencja Potęgi | 10 % do wszystkich statystyk | data-buff= 51 
var bb18 = false;    //Zaklęta esencja Mocy | 25 % do ilości mocy z walk PvM | data-buff= 50 
var bb19 = false;    //Zaklęta esencja Doświadczenia | 500 % do doświadczenia | data-buff= 52 

const blessingsHtml = `
<section id="Blessings_Panel">
  <h4>Błogosławieństwa</h4>
  <div class="dropdown">
    <button class="dropdown-toggle" onclick="toggleDropdown('exp')">EXP</button>
    <div class="options exp">
      <button data-index="1" onclick="toggleBlessing(1)">Błogosławieństwo Widmowego Smoka</button>
      <button data-index="2" onclick="toggleBlessing(2)">Grzech Pychy</button>
      <button data-index="3" onclick="toggleBlessing(3)">Grzech Obżarstwa</button>
      <button data-index="4" onclick="toggleBlessing(4)">Grzech Pożądania</button>
      <button data-index="5" onclick="toggleBlessing(5)">Grzech Lenistwa</button>
      <button data-index="6" onclick="toggleBlessing(6)">Grzech Chciwości</button>
      <button data-index="8" onclick="toggleBlessing(8)">Grzech Gniewu</button>
      <button data-index="9" onclick="toggleBlessing(9)">Błogosławieństwo Leniwej Boginii</button>
      <button data-index="10" onclick="toggleBlessing(10)">Błogosławieństwo Szalonej Bogini</button>
      <button data-index="14" onclick="toggleBlessing(14)">Zaklęta esencja mocy</button>
      <button data-index="16" onclick="toggleBlessing(16)">Zaklęta esencja szczęścia</button>
      <button data-index="18" onclick="toggleBlessing(18)">Zaklęta esencja Mocy</button>
      <button data-index="19" onclick="toggleBlessing(19)">Zaklęta esencja Doświadczenia</button>
      <!-- Pozostałe opcje EXP... -->
    </div>
  </div>
  <div class="dropdown">
    <button class="dropdown-toggle" onclick="toggleDropdown('tren')">TREN</button>
    <div class="options tren">
      <button data-index="11" onclick="toggleBlessing(11)">Błogosławieństwo Życzliwej Bogini</button>
      <button data-index="15" onclick="toggleBlessing(15)">Zaklęta esencja inspiracji</button>
    </div>
  </div>
  <div class="dropdown">
    <button class="dropdown-toggle" onclick="toggleDropdown('pvp')">PVP</button>
    <div class="options pvp">
      <button data-index="12" onclick="toggleBlessing(12)">Błogosławieństwo Legendarnego Bohatera</button>
      <button data-index="13" onclick="toggleBlessing(13)">Błogosławieństwo Boga Wojny</button>
      <button data-index="7" onclick="toggleBlessing(7)">Grzech Zazdrości</button>
    </div>
  </div>
</section>
`;

document.body.insertAdjacentHTML('afterbegin', blessingsHtml);


const blessingsCss = `
#Blessings_Panel {
  position: fixed;
  right: 35px;
  top: 114px;
  z-index: 9999;
  width: 300px;
  background-color: rgba(0, 0, 0, 0.9);
  color: white;
  border-radius: 5px;
  padding: 10px;
  cursor: move;
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 10px;
}
  
  #Blessings_Panel h4 {
    margin: 0;
  }
  
  .dropdown-toggle {
    background-color: transparent;
    border: none;
    color: white;
    cursor: pointer;
    grid-column: 1;
  }
  
  .options {
    display: none;
  }
  
  .options.exp.open,
  .options.tren.open,
  .options.pvp.open {
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
    grid-gap: 1px;
  }
  
  .options button {
    background-color: rgba(255, 255, 255, 0.1);
    border: none;
    color: white;
    padding: 5px;
    width: 100%;
    cursor: pointer;
  }
  
  .options button:hover {
    background-color: rgba(255, 255, 255, 0.2);
  }
  
  .options button.active {
    background-color: #34979f;
  }
.options button.selectAll {
  background-color: rgba(0, 255, 0, 0.1);
}

.options button.selectAll:hover {
  background-color: rgba(0, 255, 0, 0.2);
}
`;

const blessingsStyleElement = document.createElement('style');
blessingsStyleElement.textContent = blessingsCss;
document.head.appendChild(blessingsStyleElement);

function toggleBlessing(index) {
    const bbVariable = 'bb' + index;
    window[bbVariable] = !window[bbVariable];
    const button = document.querySelector(`button[data-index="${index}"]`);
    button.classList.toggle('active', window[bbVariable]);
}

function toggleDropdown(type) {
    const options = document.querySelector(`.options.${type}`);
    options.classList.toggle('open');
}
let isblessdragging = false;
let mouseOffsetX, mouseOffsetY;

$(document).on('mousedown', '#Blessings_Panel', handleMouseDown);
$(document).on('mousemove', handleMouseMove);
$(document).on('mouseup', handleMouseUp);

function handleMouseDown(event) {
    isblessdragging = true;
    mouseOffsetX = event.clientX - $('#Blessings_Panel').offset().left;
    mouseOffsetY = event.clientY - $('#Blessings_Panel').offset().top;
}

function handleMouseMove(event) {
    if (isblessdragging) {
        const left = event.clientX - mouseOffsetX;
        const top = event.clientY - mouseOffsetY;
        $('#Blessings_Panel').css('left', left + 'px');
        $('#Blessings_Panel').css('top', top + 'px');
    }
}

function handleMouseUp() {
    isblessdragging = false;
}

var Grzechy_spr = {
    paused: false
};

let turnOnGrzechy = setInterval(() =>{
    if(Grzechy_spr.paused==false){
        grzechy();
    }
    else{

    }
},1000 *30 *1)
function grzechy() {
    if (Grzechy_spr.paused) {
        console.log('Wpisuje koda prawdopodobnie');
    } else {
        const buffConditions = [
            { condition: bb1, buffId: 100, itemId: 1801 },
            { condition: bb2, buffId: 96, itemId: 1796 },
            { condition: bb3, buffId: 95, itemId: 1795 },
            { condition: bb4, buffId: 94, itemId: 1794 },
            { condition: bb5, buffId: 93, itemId: 1793 },
            { condition: bb6, buffId: 92, itemId: 1792 },
            { condition: bb7, buffId: 91, itemId: 1791 },
            { condition: bb8, buffId: 90, itemId: 1790 },
            { condition: bb9, buffId: 82, itemId: 1753 },
            { condition: bb10, buffId: 81, itemId: 1752 },
            { condition: bb11, buffId: 80, itemId: 1751 },
            { condition: bb12, buffId: 73, itemId: 1744 },
            { condition: bb13, buffId: 71, itemId: 1742 },
            { condition: bb14, buffId: 55, itemId: 1630 },
            { condition: bb15, buffId: 54, itemId: 1629 },
            { condition: bb16, buffId: 53, itemId: 1628 },
            //{ condition: bb17, buffId: 51, itemId: /*Wstaw itemId dla tego warunku*/ },
            { condition: bb18, buffId: 50, itemId: 1559 },
            { condition: bb19, buffId: 52, itemId: 1608 },
            // Dodaj kolejne warunki...
        ];

        for (const condition of buffConditions) {
            if (condition.condition) {
                const buffElement = $("#char_buffs").find(`div[data-buff=${condition.buffId}]`);
                const buffTimer = buffElement.find(".timer").text();

                if (buffTimer <= '00:00:10' || buffElement.length === 0) {
                    replace(condition.itemId);
                    return; // Wyjście z funkcji po znalezieniu i obsłużeniu pierwszego spełnionego warunku
                }
            }
        }

        if ($("#tsr_controlPanel").find("button").text() === 'START') {
            $("#tsr_controlPanel").find("button").click();
        } else if ($(".gh_button.gh_pvp").text() !== 'undefined' && $(".gh_button.gh_pvp").text() == 'PVP Off') {
            $(".gh_button.gh_pvp").click()
            
        }
        console.log('As you wish my Lord');
    }
}
function switchPages() {
      setTimeout(function () {
            GAME.socket.emit('ga', {
                        a: 12,
                        page: 10
                    });
            czekaj();
        }, 500);
}

function replace(item_id) {
    setTimeout(function () {
        let buffID = parseInt($("#ekw_page_items").find(`div[data-base_item_id='${item_id}']`).attr("data-item_id"));
        if (isNaN(parseFloat(buffID))) {
            switchPages();
        }
        else {
            sendEmit(buffID);
        }
    }, 80);
}
function sendEmit(buffID) {
    if ($("#tsr_controlPanel").find("button").text() == 'STOP') {
        $("#tsr_controlPanel").find("button").click();
    }
    else if ($(".gh_button.gh_pvp").text() !== 'undefined' && $(".gh_button.gh_pvp").text() == 'PVP On') {
            $(".gh_button.gh_pvp").click()
            
        }
    setTimeout(function () {
        GAME.socket.emit('ga', { a: 12, type: 14, iid: buffID, page: 10});
        czekaj();
    }, 1100);
}

function czekaj() {
    setTimeout(function () {
        grzechy();
    }, 400);
}

czekaj();
